﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraControl : MonoBehaviour
{
    public GameObject player;
    public GameObject mainCamera;
    public GameObject targetPoint;
    [Range(3,8)]public float distance;
    public float sensitivity;
    public float startAngle;
    [SerializeField]private uint mode;

    private Vector3 static_direction;
    private float FPS_VerticalDegree;
    private float TPS_VerticalDegree;
    private float TPS_HorizontalDegree;
    private Vector3 direction;
    private float _distance;
    // Start is called before the first frame update
    void Start()
    {
        

    }
    private void OnEnable()
    {
        static_direction = -player.transform.forward;
        TPS_VerticalDegree = startAngle;
    }
    private void FixedUpdate()
    {

        //mode = control._mode;
        if (mode==1)
        {
            //第一人稱射擊
            CameraFPS_InPutControl();
            CameraFPS_Direction();
            CameraFPS_Move();
            
        }
        else if (mode==0)
        {

            //第三人稱射擊
            //CameraTPS_InPutControl();
            CameraTPS_Joysticks_InPutControl();
            CameraTPS_Move();
        }
        



    }
    // Update is called once per frame
    void Update()
    {
        if (mode==1)
        {

        }
        else if (mode==0)
        {
            CameraTPS_Direction();
        }
    }



    //相機永遠看向player
    void CameraTPS_Direction()
    {
        mainCamera.transform.rotation = Quaternion.LookRotation(player.transform.position - mainCamera.transform.position, Vector3.up);
    }
    void CameraTPS_InPutControl()
    {
        if (Input.GetMouseButton(0))
        {
            TPS_HorizontalDegree += Input.GetAxis("Mouse X") * sensitivity;
            TPS_VerticalDegree -= Input.GetAxis("Mouse Y") * sensitivity;
            Angle_Control();
        }
    }
    void CameraTPS_Joysticks_InPutControl()
    {
        
        TPS_HorizontalDegree += Input.GetAxis("RightStickX") * 1f;
        TPS_VerticalDegree += Input.GetAxis("RightStickY") * 1f;
        Angle_Control();
    }

    void CameraTPS_Move()
    {
        _distance = distance;
        direction = Quaternion.Euler(TPS_VerticalDegree, TPS_HorizontalDegree, 0f) * static_direction;
        Collider_Check();
        mainCamera.transform.position = player.transform.position + direction * _distance;
        
    }



    void CameraFPS_Direction()
    {
        mainCamera.transform.rotation = player.transform.rotation* Quaternion.Euler(FPS_VerticalDegree, 0F, 0f);
    }
    void CameraFPS_InPutControl()
    {
        if (Input.GetMouseButton(0))
        {
            FPS_VerticalDegree -= Input.GetAxis("Mouse Y") * sensitivity;
            //Debug.Log(FPS_VerticalDegree);
            FPS_VerticalDegree = Mathf.Clamp(FPS_VerticalDegree, -80, 38);
        }
    }
    void CameraFPS_Move()
    {

        mainCamera.transform.position = player.transform.position+player.transform.forward*0.5f+transform.up*0.5f;

    }
    void Collider_Check()
    {
        RaycastHit hf;
        if (Physics.SphereCast(player.transform.position, 0.5f, direction, out hf, distance))
        {
            //Debug.Log(hf.distance);
            _distance = hf.distance;
        }
    }
    void Angle_Control()
    {
        TPS_VerticalDegree = Mathf.Clamp(TPS_VerticalDegree, -55, 80);
        //Debug.Log("TPS_VerticalDegree:"+TPS_VerticalDegree);
        TPS_HorizontalDegree = TPS_HorizontalDegree < 360 ? TPS_HorizontalDegree : (TPS_HorizontalDegree - 360);
    }
    void CameraControl2()
    {
        Quaternion x = Quaternion.LookRotation(targetPoint.transform.position - mainCamera.transform.position);
        mainCamera.transform.rotation = x;
    }
}
