﻿//#define keyboard
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player_Controller 
{
    #region variable
    //player collider
    //singleton
    private static Player_Controller _Player;

    
    /// <summary>
    /// 現在控制腳色
    /// </summary>
    public GameObject Now_Player;
    public GameObject AI_Player;
    public GameObject AI2_Player;
    private GameObject change_Player;
    /// <summary>
    /// 跟隨攝影機、不要改他
    /// </summary>
    public Transform follow_Camera;
    /// <summary>
    /// 現在鎖定目標、等到資源持寫完用來偵測腳色一段距離內的怪物用來鎖定
    /// </summary>
    public GameObject target;
    /// <summary>
    /// 移動速度
    /// </summary>
    public float speed=10f;
    /// <summary>
    ///enable時的攝影機初始角度、不要改他 
    /// </summary>
    public float startAngle=30f;
    /// <summary>
    ///攝影機與player之間的最大距離，不要改他 
    /// </summary>
    [Range(3, 8)] public float Max_Distance=4F;
    //允許地面最大斜度
    public float enableGroundAngle=50f;


    //現在控制的腳色transform
    private Transform Control_Transform;
    //現在控制腳色animator
    public Animator Now_Player_animator;
    //一般控制模式(false)或鎖定模式(true)
    private bool control_mode;
    //joystick方向，不是角色方向
    private Vector3 joystick_forward;
    private Vector3 joystick_right;
    //腳色移動射線layer mask
    private int playermask=1<<0;

    //攝影機初始方向
    private Vector3 static_direction;
    //攝影機角度
    private float TPS_VerticalDegree;
    private float TPS_HorizontalDegree;
    //攝影機的實際距離
    private float _distance;
    //攝影機的實際方向
    private Vector3 direction;
    // 
    private Vector3 FFT_Height = new Vector3(0f, 1.5f, 0f);

    //是否有用技能
    private bool Skill;
    //攝影機smooth參數
    private Vector3 Fake_FollowTarget;
    //相機射線layer
    private int CameraMask=1<<0|0<<9|0<<10;
    #endregion 
    //記住初始化的先後順序

    void Start()
    {
        //Debug.Log("startpostition:" + now_Player.transform.position);
    }
    public void Player_OnEnable()
    {

        control_mode = false;
        move = TPS_Move;
        cameraMove = Camera_TPS_Move;
        static_direction = Now_Player.transform.forward;
        TPS_VerticalDegree = startAngle;
        direction = Quaternion.Euler(TPS_VerticalDegree, TPS_HorizontalDegree, 0f) * static_direction;
        Fake_FollowTarget = Now_Player.transform.position + FFT_Height;
        _distance = Max_Distance;


        Control_Transform = Now_Player.transform;
        Now_Player_animator = Now_Player.GetComponent<Animator>();
        Skill = false;
        //cur_Player = transform.GetChild(0).gameObject;

    }

    public void Player_FixedUpdate()
    {
        move();
        cameraMove();
    }
    public void Player_Update()
    {
        KeyChange_Check();
        GroundCheck();


    }
    //判斷所有輸入變化
    #region keycheck
    void KeyChange_Check()
    {
        SkillCheck();
        if (Skill)
        {
            SkillAttackCheck();
        }
        else
        {
            TriangeCheck();
            SquareCheck();
            Jump_Check();
            AttckCheck();
        }

    }
    //判斷三角鍵模式變化
    private bool battle;
    void TriangeCheck()
    {
#if keyboard
        if (Input.GetKeyDown(KeyCode.Z))
#else
        if (Input.GetKeyDown(KeyCode.Joystick1Button3))
#endif
        {
            
            battle = !battle;
            Now_Player_animator.SetBool("battle", battle);
            
            //control_mode = !control_mode;
            //Mode_Change();
        }
    }
    void SquareCheck()
    {
#if keyboard
        if (Input.GetKeyDown(KeyCode.Q))
#else
        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
#endif
        {
            Change_Player();
        }

    }

    void Jump_Check()
    {
#if keyboard
        if (Input.GetKeyDown(KeyCode.Space) )
#else
        if (Input.GetKeyDown(KeyCode.Joystick1Button1) )
#endif
        {
            Now_Player_animator.SetTrigger("jump");
        }
    }
    void AttckCheck()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button2))
        {
            Now_Player_animator.SetTrigger("NormalAttack");
        }
    }

    void SkillAttackCheck()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            Now_Player_animator.SetTrigger("Spinkick");
        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            Now_Player_animator.SetTrigger("RISING_P");

        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button2))
        {
            Now_Player_animator.SetTrigger("Hikick");

        }
        else if (Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
            Now_Player_animator.SetTrigger("ScrewKick");

        }
        
        
            
        
        
        
    }
    void SkillCheck()
    {
        if (Input.GetKey(KeyCode.Joystick1Button5))
        {
            Skill = true;
        }
        else
        {
            Skill = false;
        }
        
    }
    void Change_Player()
    {
        //取出現在位置方向
        Vector3 position = Control_Transform.position;
        Vector3 forward = Control_Transform.forward;
        //將現在使用變數指定的地址變換
        change_Player = Now_Player;
        Now_Player = AI_Player;
        AI_Player = AI2_Player;
        AI2_Player = change_Player;
        change_Player = null;

        //將原本AI1(現在Player)的位置方向丟給原本Player(現在AI2)的位置方向
        AI2_Player.transform.position = Now_Player.transform.position;
        AI2_Player.transform.forward = Now_Player.transform.forward;

        //將Control的位置方向丟給現在Player
        Now_Player.transform.position = position;
        Now_Player.transform.forward = forward;
        //指定Control為現在Player
        Control_Transform = Now_Player.transform;
        Now_Player_animator = Now_Player.GetComponent<Animator>();



    }

    //
    private void Mode_Change()
    {
        if (control_mode)
        {
            move = Target_Move;
            cameraMove = Camera_Target_Move;
        }
        else
        {
            move = TPS_Move;
            cameraMove = Camera_TPS_Move;
        }
    }
#endregion
#region playercontrol
    private Action move;
    //兩種動作模式
    private void TPS_Move()
    {
        joystickForward();
        move_josystick();
    }
    private void Target_Move()
    {

    }



    //TPS相關公式
    /// <summary>
    /// 負責遊戲控制器左邊移動的方向，
    /// </summary>
    private void joystickForward()
    {
        //指定前方
        joystick_forward = follow_Camera.forward;
        joystick_forward.y = 0;
        joystick_forward.Normalize();
        //指定右方
        joystick_right =  Quaternion.AngleAxis(90f, Vector3.up)*joystick_forward;
        joystick_right.Normalize();
        //now_Player.transform.forward = _forward;
    }
    /// <summary>
    /// 負責腳色位置的移動
    /// </summary>
    private void move_josystick()
    {
        Vector3 now_position = Control_Transform.position;
        Vector3 next_position = now_position;
        float Hor_Input=0f;
        float Ver_Input=0f;
        if (Mathf.Abs(Input.GetAxis("Horizontal")) >0.05f)
        {
            Hor_Input = Input.GetAxis("Horizontal");
        }
        if (Mathf.Abs(Input.GetAxis("Vertical")) > 0.05f)
        {
            Ver_Input = Input.GetAxis("Vertical");
        }
        //Debug.Log("Hor_Input:" + Hor_Input);
        //Debug.Log("Ver_Input:" + Ver_Input);
        next_position += (Hor_Input * joystick_right* speed+ Ver_Input * joystick_forward * speed) * Time.fixedDeltaTime;
        //Debug.Log("Input.GetAxis(Horizontal) :" + Input.GetAxis("Horizontal"));

        Animaiton_forward(now_position,next_position);
        float animation_speed = Mathf.Sqrt(Hor_Input * Hor_Input + Ver_Input * Ver_Input);
        Now_Player_animator.SetFloat("speed", animation_speed);


        bool xzcheck = XZCheck(now_position, next_position);
        //如果地面檢測沒過則xz方向不移動
        if (!xzcheck)
        {
            next_position = now_position;
        }
        
        //跳躍
        

        Control_Transform.position = next_position;
    }
    bool XZCheck(Vector3 now_positon, Vector3 next_position)
    {
        //測試下個位置打不打的到地板的點來確定是否可移動
        RaycastHit rh;
        //現在位置地面點
        RaycastHit rnow;
        //下個位置偵測起始點
        Vector3 rayheight= new Vector3(0f, 0.3f, 0f);
        Vector3 detectPoint=next_position+rayheight;
        Vector3 nowdetctPoint = now_positon+rayheight;
        Physics.Raycast(nowdetctPoint, -Vector3.up, out rnow, 100.0f,9);
        bool groundcheck= Physics.Raycast(detectPoint, -Vector3.up, out rh, 100.0f, playermask);
        
        
        //如果下個點有打到地板
        if (groundcheck)
        {
            //下個點跟現在點之間的向量
            Vector3 next= rh.point - rnow.point;
            float angle = Mathf.Asin(next.y / next.magnitude) * Mathf.Rad2Deg;

            if ( angle<=enableGroundAngle)
            {
                return true;
            }
            else if (angle > enableGroundAngle)
            {
                Debug.Log("下個點的移動角度大於上限");
                Debug.Log("angle:" + angle);
                Debug.Log("next:" + next);
                Debug.Log("rh:" + rh.point);
                Debug.Log("rnow:" + rnow.point);
                return false;
            }
            return false;
        }
        Debug.Log("下個點沒有打到地板");
        Debug.Log("detect:" + detectPoint);
        Debug.Log("rh:" + rh.point);
        Debug.Log("rnow:"+ rnow.point);
        return false;

    }

    
    
    /// <summary>
    /// 決定腳色方向
    /// </summary>
    /// <param name="nowposition"></param>
    /// <param name="nextposition"></param>
    void Animaiton_forward(Vector3 nowposition,Vector3 nextposition)
    {
        Vector3 model_forward = nextposition - nowposition;
        //Debug.Log(model_forward);
        if ( model_forward.magnitude> 0f )
        {
            model_forward.Normalize();
            Control_Transform.forward = Vector3.Lerp(Control_Transform.forward,model_forward,Time.fixedDeltaTime*20f);
        }
    }
    
#endregion
#region cameracontrol
    private Action cameraMove;
    void Camera_TPS_Move()
    {
        CameraTPS_Joysticks_InPutControl();
        CameraTPS_Move();
    }
    void Camera_Target_Move()
    {

    }
    void CameraTPS_Joysticks_InPutControl()
    {

        TPS_HorizontalDegree += Input.GetAxis("RightStickX") * 1f;
        TPS_VerticalDegree += Input.GetAxis("RightStickY") * 1f;
        Angle_Control();
    }
    void Angle_Control()
    {
        TPS_VerticalDegree = Mathf.Clamp(TPS_VerticalDegree, -40, 55);
        TPS_HorizontalDegree = TPS_HorizontalDegree < 360 ? TPS_HorizontalDegree : (TPS_HorizontalDegree - 360);
        //Debug.Log("TPS_VerticalDegree:"+ TPS_VerticalDegree);
        //Debug.Log("TPS_HorizontalDegree:" + TPS_HorizontalDegree);
        
    }
    void CameraTPS_Move()
    {
        Vector3 next_position = Control_Transform.position + FFT_Height;
        direction = Quaternion.Euler(TPS_VerticalDegree, TPS_HorizontalDegree, 0f) * static_direction;
        Camera_Lerp(next_position);
        CameraTPS_Direction(Fake_FollowTarget);
        Collider_Check(Fake_FollowTarget);
        follow_Camera.position = Fake_FollowTarget - direction * _distance;
        Debug.DrawLine(Fake_FollowTarget, Fake_FollowTarget- direction * _distance,Color.red);
        //+ new Vector3(0f, 2f, 0f) 
        //
    }
    //攝影機碰撞偵測
    void Collider_Check(Vector3 position)
    {
        RaycastHit hf;
        if (Physics.SphereCast(position, 0.5f, -direction, out hf, Max_Distance, CameraMask))
        {
            _distance = Mathf.Lerp(_distance, hf.distance, Time.fixedDeltaTime*10f);
            _distance = _distance > 1f ? _distance : 1f;
        }
        else
        {
            _distance = Mathf.Lerp(_distance,Max_Distance,Time.fixedDeltaTime);
        }
    }
    void CameraTPS_Direction(Vector3 position)
    {
        Vector3 Look_Direction= position - follow_Camera.position;
        follow_Camera.rotation = Quaternion.LookRotation(Look_Direction, Vector3.up);
    }
    void Camera_Lerp(Vector3 position)
    {

        position = Vector3.Lerp(Fake_FollowTarget, position, Time.fixedDeltaTime * 4f);
        Fake_FollowTarget = position;
        //Debug.Log("Check_Distance(Fake_FollowTarget,position):"+Check_Distance(Fake_FollowTarget, position));
        if (Check_Distance(Fake_FollowTarget,position))
        {

            
            
            //Debug.Log("Fake_FollowTarget:" + Fake_FollowTarget);
            //Debug.Log("nextpoint:" + position);
        }
    }
    bool Check_Distance(Vector3 now_postion,Vector3 next_postion)
    {
        return Vector3.Distance(now_postion, next_postion) > 1f;
    }
#endregion

    void GroundCheck()
    {
        //測試地面判定
        RaycastHit rh;
        Physics.Raycast(Control_Transform.position, -Vector3.up, out rh, 100.0f, playermask);
        if (rh.distance > 0.8f)
        {
            Now_Player_animator.SetBool("air", true);
            Now_Player_animator.SetBool("land", false);
        }
        else if (  0.8f>=rh.distance&&rh.distance > 0.1f)
        {
            Now_Player_animator.SetBool("air", true);
            Now_Player_animator.SetBool("land", true);
        }
        else if (rh.distance <= 0.1f)
        {
            Now_Player_animator.SetBool("land", false);
            Now_Player_animator.SetBool("air", false);
        }
    }

    public void Player_OnDrawGizmos()
    {
            
        Gizmos.DrawSphere(Fake_FollowTarget,0.2f);
        Gizmos.DrawSphere(Fake_FollowTarget - direction * _distance, 0.05f);
    }
}
