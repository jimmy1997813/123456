﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnityChanAnimation_event : MonoBehaviour
{
    Animator _animator;
    private void Start()
    {
        _animator = GetComponent<Animator>();
    }
    void falling()
    {
        _animator.SetBool("falling",true);
    }
    void land()
    {
        _animator.SetBool("falling", false);
    }
}

