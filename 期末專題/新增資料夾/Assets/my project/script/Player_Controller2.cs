﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player_Controller2
{
    #region variable
    //player collider
    //singleton
    private static Player_Controller _Player;


    /// <summary>
    /// 現在控制腳色
    /// </summary>
    public GameObject Now_Player;
    public GameObject AI_Player;
    public GameObject AI2_Player;
    private GameObject change_Player;
    /// <summary>
    /// 跟隨攝影機、不要改他
    /// </summary>
    public Transform follow_Camera;
    /// <summary>
    /// 現在鎖定目標、等到資源持寫完用來偵測腳色一段距離內的怪物用來鎖定
    /// </summary>
    public GameObject target;
    /// <summary>
    /// 移動速度
    /// </summary>
    public float speed = 10f;
    /// <summary>
    ///enable時的攝影機初始角度、不要改他 
    /// </summary>
    public float startAngle = 30f;
    /// <summary>
    ///攝影機與player之間的最大距離，不要改他 
    /// </summary>
    [Range(3, 8)] public float Max_Distance = 5F;
    //允許地面最大斜度
    public float enableGroundAngle = 50f;


    //現在控制的腳色transform
    private Transform Control_Transform;
    //現在控制腳色animator
    public Animator Now_Player_animator;
    //一般控制模式(false)或鎖定模式(true)
    private bool control_mode;
    //joystick方向，不是角色方向
    private Vector3 joystick_forward;
    private Vector3 joystick_right;

    //攝影機初始方向
    private Vector3 static_direction;
    //攝影機角度
    private float TPS_VerticalDegree;
    private float TPS_HorizontalDegree;
    //攝影機的實際距離
    private float _distance;
    //攝影機的實際方向
    private Vector3 direction;

    //二段跳次數參數
    private uint jump_time;
    //現在離地面高度
    private float jumphelight;
    //在空中還是在地面
    private bool air;
    //要跳躍距離
    private float Jump_move;
    //重力向下速度
    private float gravity = -9.8f;
    //跳耀速度
    private float jumpspeed = 1f;



    //攝影機smooth參數
    private Vector3 Fake_FollowTarget;

    #endregion 
    //記住初始化的先後順序

    void Start()
    {
        //Debug.Log("startpostition:" + now_Player.transform.position);
    }
    public void Player_OnEnable()
    {

        control_mode = false;
        move = TPS_Move;
        cameraMove = Camera_TPS_Move;
        static_direction = Now_Player.transform.forward;
        TPS_VerticalDegree = startAngle;
        direction = Quaternion.Euler(TPS_VerticalDegree, TPS_HorizontalDegree, 0f) * static_direction;
        Fake_FollowTarget = Now_Player.transform.position;

        Control_Transform = Now_Player.transform;
        Now_Player_animator = Now_Player.GetComponent<Animator>();
        //cur_Player = transform.GetChild(0).gameObject;

    }

    public void Player_FixedUpdate()
    {
    }
    public void Player_Update()
    {
        KeyChange_Check();
        move();
        cameraMove();

        //測試現在是否浮空


    }
    //判斷所有輸入變化
    #region keycheck
    void KeyChange_Check()
    {
        TriangeCheck();
        SquareCheck();
        SkillCheck();
    }
    //判斷三角鍵模式變化
    void TriangeCheck()
    {
        if (Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
            control_mode = !control_mode;
            Mode_Change();
        }
    }
    void SquareCheck()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Change_Player();
        }

    }
    void XCheck()
    {
        if (Input.GetKeyDown(KeyCode.R) && jump_time < 2)
        {
            jump_time++;
            Jump_move = 1f;
            Now_Player_animator.SetBool("air", true);
        }

    }


    void SkillCheck()
    {

        if (Input.GetKey(KeyCode.E))
        {
            Now_Player_animator.SetTrigger("skill1");
        }
    }
    void Change_Player()
    {
        //取出現在位置方向
        Vector3 position = Control_Transform.position;
        Vector3 forward = Control_Transform.forward;
        //將現在使用變數指定的地址變換
        change_Player = Now_Player;
        Now_Player = AI_Player;
        AI_Player = AI2_Player;
        AI2_Player = change_Player;
        change_Player = null;

        //將原本AI1(現在Player)的位置方向丟給原本Player(現在AI2)的位置方向
        AI2_Player.transform.position = Now_Player.transform.position;
        AI2_Player.transform.forward = Now_Player.transform.forward;

        //將Control的位置方向丟給現在Player
        Now_Player.transform.position = position;
        Now_Player.transform.forward = forward;
        //指定Control為現在Player
        Control_Transform = Now_Player.transform;
        Now_Player_animator = Now_Player.GetComponent<Animator>();



    }

    //
    private void Mode_Change()
    {
        if (control_mode)
        {
            move = Target_Move;
            cameraMove = Camera_Target_Move;
        }
        else
        {
            move = TPS_Move;
            cameraMove = Camera_TPS_Move;
        }
    }
    #endregion
    #region playercontrol
    private Action move;
    //兩種動作模式
    private void TPS_Move()
    {
        joystickForward();
        move_josystick();
    }
    private void Target_Move()
    {

    }



    //TPS相關公式
    /// <summary>
    /// 負責遊戲控制器左邊移動的方向，
    /// </summary>
    private void joystickForward()
    {
        //指定前方
        joystick_forward = follow_Camera.forward;
        joystick_forward.y = 0;
        joystick_forward.Normalize();
        //指定右方
        joystick_right = Quaternion.AngleAxis(90f, Vector3.up) * joystick_forward;
        joystick_right.Normalize();
        //now_Player.transform.forward = _forward;
    }
    /// <summary>
    /// 負責腳色位置的移動
    /// </summary>
    private void move_josystick()
    {
        Vector3 now_position = Control_Transform.position;
        Vector3 next_position = now_position;
        float Hor_Input = Input.GetAxis("Horizontal");
        float Ver_Input = Input.GetAxis("Vertical");
        next_position += (Hor_Input * joystick_right * speed + Ver_Input * joystick_forward * speed) * Time.fixedDeltaTime;
        //Debug.Log("Input.GetAxis(Horizontal) :" + Input.GetAxis("Horizontal"));

        Animaiton_forward(now_position, next_position);
        float animation_speed = Mathf.Sqrt(Hor_Input * Hor_Input + Ver_Input * Ver_Input);
        Now_Player_animator.SetFloat("speed", animation_speed);

        //如果地面檢測沒過則xz方向不移動
        next_position = XYZCheck(now_position, next_position);

        Control_Transform.position = next_position;
    }
    Vector3 XYZCheck(Vector3 now_positon, Vector3 next_position)
    {

        //下個位置地面點
        RaycastHit rh;
        //現在位置地面點
        RaycastHit rnow;
        //下個位置偵測起始點
        Vector3 rayheight = new Vector3(0f, 5f, 0f);
        Vector3 detectPoint = next_position + rayheight;
        Vector3 now_detectPoint = now_positon + rayheight;

        //打出現在位置射線
        Physics.Raycast(now_detectPoint, -Vector3.up, out rnow, 100.0f, 9);
        //下個位置
        bool groundcheck = Physics.Raycast(detectPoint, -Vector3.up, out rh, 100.0f, 9);
        Debug.DrawLine(detectPoint, next_position - Vector3.up * 10f);

        bool range = rh.distance - rayheight.y <= 0.1f;

        //如果落地，跳躍次數歸0
        if (range)
        {
            jump_time = 0;
            air = false;
        }
        else
        {
            air = true;
        }
        Debug.Log("range:" + range);
        Debug.Log("rnow:" + rnow.point);
        //測試下個位置打不打的到地板的點來確定可否進行XZ方向移動
        //如果下個點有打到地板
        if (groundcheck)
        {
            //下個點跟現在點之間的向量
            Vector3 next = rh.point - rnow.point;
            float angle = Mathf.Asin(next.y / next.magnitude) * Mathf.Rad2Deg;

            if (next.magnitude == 0F)
            {
                angle = 0F;
            }
            //用來檢查地形角度
            //Debug.Log(angle);
            if (angle > enableGroundAngle)
            {
                Debug.Log("下個點的移動角度大於上限");
                Debug.Log("angle:" + angle);
                Debug.Log("next:" + next);
                Debug.Log("rh:" + rh.point);
                Debug.Log("rnow:" + rnow.point);
                next_position = now_positon;
            }
            else if (angle < enableGroundAngle && air == false)
            {
                //next_position.y = rh.point.y;
            }


        }
        else
        {
            Debug.Log("下個點沒有打到地板");
            Debug.Log("detect:" + detectPoint);
            Debug.Log("rh:" + rh.point);
            Debug.Log("rnow:" + rnow.point);
            next_position = now_positon;
        }

        //Y軸處理


        //Debug.Log("air:" + air);

        if (air)
        {
            if (Jump_move > 0f)
            {
                float Next_YMove = jumpspeed * Time.deltaTime;
                if (Jump_move > Next_YMove)
                {
                    next_position.y += Next_YMove;
                    Jump_move -= Next_YMove;
                }
                else
                {
                    next_position.y += Jump_move;
                    Jump_move = 0f;
                }
            }
            else
            {
                float Next_YMove = gravity * Time.deltaTime;
                if (rh.distance > Mathf.Abs(Next_YMove))
                {
                    next_position.y += Next_YMove;
                }
                else
                {
                    next_position.y += -rh.distance;
                }
            }
        }
        else
        {
            if (Jump_move > 0f)
            {
                float Next_YMove = jumpspeed * Time.deltaTime;
                if (Jump_move > Next_YMove)
                {
                    next_position.y += Next_YMove;
                    Jump_move -= Next_YMove;
                }
                else
                {
                    next_position.y += Jump_move;
                    Jump_move = 0f;
                }
            }
        }




        //Debug.Log("2next_position.y:" + next_position.y);
        return next_position;
    }

    /// <summary>
    /// 自寫落地偵測
    /// </summary>
    /// <param name="next_position"></param>
    void YGroundCheck(Vector3 next_position)
    {
        RaycastHit rh;
        Physics.Raycast(next_position, -Vector3.up, out rh, 100.0f);
        //Debug.Log(rh.distance);
        //如果落第距離在多少以下是為切出地面，jump次數歸0
        if (rh.distance <= 1.7f)
        {
            jump_time = 0;
        }
    }
    /// <summary>
    /// 決定腳色方向
    /// </summary>
    /// <param name="nowposition"></param>
    /// <param name="nextposition"></param>
    void Animaiton_forward(Vector3 nowposition, Vector3 nextposition)
    {
        Vector3 model_forward = nextposition - nowposition;
        //Debug.Log(model_forward);
        if (model_forward.magnitude > 0f)
        {

            Control_Transform.forward = model_forward;
        }
    }

    #endregion
    #region cameracontrol
    private Action cameraMove;
    void Camera_TPS_Move()
    {
        CameraTPS_Joysticks_InPutControl();
        CameraTPS_Move();
    }
    void Camera_Target_Move()
    {

    }
    void CameraTPS_Joysticks_InPutControl()
    {

        TPS_HorizontalDegree += Input.GetAxis("RightStickX") * 1f;
        TPS_VerticalDegree += Input.GetAxis("RightStickY") * 1f;
        Angle_Control();
    }
    void Angle_Control()
    {
        TPS_VerticalDegree = Mathf.Clamp(TPS_VerticalDegree, -55, 80);
        TPS_HorizontalDegree = TPS_HorizontalDegree < 360 ? TPS_HorizontalDegree : (TPS_HorizontalDegree - 360);
        //Debug.Log("TPS_VerticalDegree:"+ TPS_VerticalDegree);
        //Debug.Log("TPS_HorizontalDegree:" + TPS_HorizontalDegree);

    }
    void CameraTPS_Move()
    {
        Vector3 next_position = Control_Transform.position + new Vector3(0f, 1f, 0f);
        _distance = Max_Distance;
        direction = Quaternion.Euler(TPS_VerticalDegree, TPS_HorizontalDegree, 0f) * static_direction;
        Collider_Check(next_position);
        Camera_Lerp(next_position);
        CameraTPS_Direction(next_position);
        Debug.DrawLine(next_position, next_position - direction * _distance, Color.red);
        //+ new Vector3(0f, 2f, 0f) 
        //
    }
    //攝影機碰撞偵測
    void Collider_Check(Vector3 position)
    {
        RaycastHit hf;
        if (Physics.SphereCast(position, 0.5f, -direction, out hf, Max_Distance, 9))
        {
            _distance = hf.distance;
        }
    }
    //
    void CameraTPS_Direction(Vector3 position)
    {
        Vector3 Look_Direction = Fake_FollowTarget - follow_Camera.position;
        follow_Camera.rotation = Quaternion.LookRotation(Look_Direction, Vector3.up);
    }
    void Camera_Lerp(Vector3 position)
    {

        //Debug.Log("Check_Distance(Fake_FollowTarget,position):"+Check_Distance(Fake_FollowTarget, position));
        if (Check_Distance(Fake_FollowTarget, position))
        {

            position = Vector3.Lerp(Fake_FollowTarget, position, Time.fixedDeltaTime * 4f);
            Fake_FollowTarget = position;
            follow_Camera.position = Fake_FollowTarget - direction * _distance;
            //Debug.Log("Fake_FollowTarget:" + Fake_FollowTarget);
            //Debug.Log("nextpoint:" + position);
        }
        else
        {
            follow_Camera.position = Fake_FollowTarget - direction * _distance;
        }
    }
    bool Check_Distance(Vector3 now_postion, Vector3 next_postion)
    {
        return Vector3.Distance(now_postion, next_postion) > 0.5f;
    }
    #endregion
    #region statecheck

    bool Air_Check()
    {
        RaycastHit rh;
        Vector3 detectPoint = Control_Transform.position;
        Physics.Raycast(detectPoint, -Vector3.up, out rh, 100f);
        if (rh.distance <= 0.1)
        {
            jump_time = 0;
            return false;
        }
        else
        {
            return true;
        }
    }

    #endregion
    private void OnDrawGizmos()
    {

        Gizmos.DrawSphere(Fake_FollowTarget, 0.2f);
    }
}
