﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllController : MonoBehaviour
{
    public Player_Controller Player_Controller;
    UnityChanAnimation_event unityChanAnimation_Event;

    private void OnEnable()
    {
        Player_Controller.Player_OnEnable();
    }
    private void FixedUpdate()
    {
        Player_Controller.Player_FixedUpdate();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Player_Controller.Player_Update();
    }
    private void OnDrawGizmos()
    {
        Player_Controller.Player_OnDrawGizmos();
    }
}
