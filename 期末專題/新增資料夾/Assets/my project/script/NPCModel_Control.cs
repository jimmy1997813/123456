﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public sealed class NPCModel_Control
{
    /// <summary>
    /// controler的playermode，用來顯示並運行現在的playermode
    /// </summary>
    public NPC cur_PlayerModel = null;
    private NPC AI1_PlayerModel = null;
    private NPC AI2_PlayerModel = null;

    /// <summary>
    /// 輸出現在的playermode，但不知道要做啥
    /// </summary>
    public NPC CurrentPlayerMode
    {
        get { return cur_PlayerModel; }
    }

    /// <summary>
    /// null建構子
    /// </summary>
    public NPCModel_Control() { }
    /// <summary>
    /// 建構子，主要用來創造新playermode，塞給原本的controller來改變playermodel
    /// </summary>
    /// <param name="ControlModel"></param>
    public NPCModel_Control(NPC ControlModel) : this()
    {
        Start(ControlModel);
    }
    private void Start(NPC model)
    {
        cur_PlayerModel = model;
        //cur_PlayerModel.SetController(this);
    }

    public void TranTo(NPC playermode)
    {
        cur_PlayerModel = playermode;
        //cur_PlayerModel.SetController(this);
    }
    public void StateUpdate()
    {

    }
}
