﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon_AI : MonoBehaviour
{
    public Nav_Data m_Data;
    FSMsystem m_FSM;
    void Start()
    {
        m_FSM = new FSMsystem(m_Data);
        m_Data.m_object = this.gameObject;
        m_Data.m_FSMSystem = m_FSM;

        D_SleepState idlestate = new D_SleepState();
        D_AlertState alertstate = new D_AlertState();
        D_FightState fightstate = new D_FightState();

        idlestate.AddTransition(eFSMTransition.Go_Alert, alertstate);

        alertstate.AddTransition(eFSMTransition.Go_Fight, fightstate);

        
        m_FSM.AddState(idlestate);
        m_FSM.AddState(alertstate);
        m_FSM.AddState(fightstate);

    }

  
    void Update()
    {
        m_FSM.DoState();
       DragonActionBehavior.SetParameterWithDragon(m_Data);
    }
}
