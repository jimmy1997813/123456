﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonActionBehavior 
{
    /// <summary>
    /// 轉向目標
    /// </summary>
    /// <param name="data"></param>
    static public void TurntoTarget(Nav_Data data)
    {

        Vector3 TargetDir = (data.m_target.transform.position - data.m_object.transform.position).normalized;
        float SingleStep = data.TurnSpeed * Time.deltaTime;
        float angle = Vector3.Angle(data.m_object.transform.forward, TargetDir);
        float needtime = angle / SingleStep;
        Quaternion t = Quaternion.LookRotation(TargetDir, Vector3.up);
        data.m_object.transform.rotation = Quaternion.Slerp(data.m_object.transform.rotation, t, SingleStep);
        //Vector3 newDir = Vector3.RotateTowards(data.m_target.transform.forward, TargetDir, SingleStep, 0.0f);
        //data.m_object.transform.rotation = Quaternion.LookRotation(newDir);
    }
    /// <summary>
    /// 計算目標是否在視野角度內
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    static public bool isTurnToTarget(Nav_Data data)
    {
        Vector3 srcLocalVect = data.m_target.transform.position - data.m_object.transform.position;
        srcLocalVect.y = 0;
        Vector3 forwardLocalPos = data.m_object.transform.forward * 1 + data.m_object.transform.position;
        Vector3 forwardLocalVect = forwardLocalPos - data.m_object.transform.position;
        forwardLocalVect.y = 0;
        float angle = Vector3.Angle(srcLocalVect, forwardLocalVect);
        if (angle < data.EyeAngleRange / 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /// <summary>
    /// 停
    /// </summary>
    /// <param name="data"></param>
    static public void AI_Idle(Nav_Data data)
    {
        data.MoveSpeed = 0;
    }
    /// <summary>
    ///檢查距離
    /// </summary>
    /// <param name="data"></param>
    /// <param name="dis"></param>
    static public float CheckDis(Nav_Data data)
    {
        data.t_Distance = Vector3.Distance(data.m_object.transform.position, data.m_target.transform.position);
        return data.t_Distance;
    }
    /// <summary>
    /// 開啟走路動畫
    /// </summary>
    /// <param name="data"></param>
    /// <param name="b"></param>
    static public void Set_D_Walk(Nav_Data data, bool b)
    {
        if (b == true)
        {
            data.animator.SetBool("TurnWalk", true);
        }
        else
        {
            data.animator.SetBool("TurnWalk", false);
        }
    }
    /// <summary>
    /// 開啟跑步動畫
    /// </summary>
    /// <param name="data"></param>
    /// <param name="b"></param>
    static public void Set_D_Run(Nav_Data data, bool b)
    {
        if (b == true)
        {
            data.animator.SetBool("TurnWalk", true);
        }
        else
        {
            data.animator.SetBool("TurnWalk", false);
        }
    }
    /// <summary>
    /// Dragon,傳送與目標的距離
    /// </summary>
    /// <param name="data"></param>
    static public void SetParameterWithDragon(Nav_Data data)
    {
        Animator ani = data.animator;

        data.t_Distance = Vector3.Distance(data.m_object.transform.position, data.m_target.transform.position);
        ani.SetFloat("t_Distance", data.t_Distance);
    }
    static public void WalkToTarget(Nav_Data data,bool b)
    {
        if (b == true)
        {
            Set_D_Walk(data, true);
            data.agent.destination = data.m_target.transform.position;
            data.agent.speed = 1.0f;
            data.agent.stoppingDistance = 5.0f;
        }
        else
        {
            Set_D_Walk(data, false);
            data.agent.speed = 0;
        }
    }
    static public void ChargeToTarget(Nav_Data data,bool b)
    {
        if (b == true)
        {
            data.agent.destination = data.m_target.transform.position;
            data.agent.speed = 4.0f;
            data.agent.stoppingDistance = 5.0f;
        }
        else
        {
            data.agent.speed = 0.0f;
        }
    }
}
