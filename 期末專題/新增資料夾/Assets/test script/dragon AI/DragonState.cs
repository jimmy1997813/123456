﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D_SleepState: FSMstate
{
    public D_SleepState()
    {
        m_StateID = eFSMStateID.SleepStateID;
    }
    public override void DoBeforeEnter(Nav_Data data)
    {
    }
    public override void DoBeforeLeave(Nav_Data data)
    {
    }
    public override void Do(Nav_Data data)
    {
        DragonActionBehavior.AI_Idle(data);
    }

    public override void CheckCondition(Nav_Data data)
    {
        if(DragonActionBehavior.CheckDis(data) <data.AlertRange)
        {
            data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Alert);
        }
    }
}
public class D_AlertState : FSMstate
{
    public D_AlertState()
    {
        m_StateID = eFSMStateID.AlertStateID;
    }
    public override void DoBeforeEnter(Nav_Data data)
    {
    }
    public override void DoBeforeLeave(Nav_Data data)
    {
    }
    public override void Do(Nav_Data data)
    {
        DragonActionBehavior.AI_Idle(data);
        DragonActionBehavior.TurntoTarget(data);
        if(DragonActionBehavior.isTurnToTarget(data) == true)
        {
            DragonActionBehavior.Set_D_Walk(data, false);
        }
        else
        {
            DragonActionBehavior.Set_D_Walk(data, true);
        }
    }

    public override void CheckCondition(Nav_Data data)
    {
        if (DragonActionBehavior.CheckDis(data) < data.d_FightRange)
        {
            data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Fight);
        }
    }
}

public class D_FightState : FSMstate
{
    public float CountTime;
    public float randnum;
    public D_FightState()
    {
        m_StateID = eFSMStateID.FightStateID;
        CountTime = 0;
        randnum = Random.Range(1, 5);
    }
    public override void DoBeforeEnter(Nav_Data data)
    {
        CountTime = 0;
    }
    public override void DoBeforeLeave(Nav_Data data)
    {
    }
    public override void Do(Nav_Data data)
    {
        CountTime += Time.deltaTime;
        if (CountTime > 3)
        {
            randnum = Random.Range(1, 10);
            Debug.Log(randnum);
            CountTime = 0;
        }
        DragonActionBehavior.TurntoTarget(data);
        if (DragonActionBehavior.isTurnToTarget(data) == true)
        {
            DragonActionBehavior.Set_D_Walk(data, false);
        }
        else
        {
            DragonActionBehavior.Set_D_Walk(data, true);
        }
        if(randnum > 7)
        {
            DragonActionBehavior.WalkToTarget(data, true);
        }
        else
        {
            DragonActionBehavior.WalkToTarget(data, false);
        }
    }

    public override void CheckCondition(Nav_Data data)
    {
       
    }
}