﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrollAI : MonoBehaviour
{
    public Nav_Data m_Data;
    FSMsystem m_FSM;

    
    void Start()
    {
        m_FSM = new FSMsystem(m_Data);
        m_Data.m_object = this.gameObject;
        m_Data.m_FSMSystem = m_FSM;
     
        M_PatrolState patrolState = new M_PatrolState();
        M_ChaseState chaseState = new M_ChaseState();
        M_AttackState attackState = new M_AttackState();
        M_AlertState alertState = new M_AlertState();
        M_DieState diestate = new M_DieState();
        M_DamageState damagestate = new M_DamageState();
        patrolState.AddTransition(eFSMTransition.Go_Alert, alertState);

        alertState.AddTransition(eFSMTransition.Go_Chase, chaseState);
        alertState.AddTransition(eFSMTransition.Go_Patrol, patrolState);

        chaseState.AddTransition(eFSMTransition.Go_Patrol, patrolState);
        chaseState.AddTransition(eFSMTransition.Go_Attack, attackState);

        attackState.AddTransition(eFSMTransition.Go_Chase, chaseState);

        damagestate.AddTransition(eFSMTransition.Go_Attack, attackState);
        damagestate.AddTransition(eFSMTransition.Go_Chase, chaseState);


        m_FSM.AddGlobalTransition(eFSMTransition.Go_Die, diestate);
        m_FSM.AddGlobalTransition(eFSMTransition.Go_Damage, damagestate);
        m_FSM.AddGlobalTransition(eFSMTransition.Go_Attack, attackState);
        m_FSM.AddGlobalTransition(eFSMTransition.Go_Chase, chaseState);

        m_FSM.AddState(patrolState);
        m_FSM.AddState(alertState);
        m_FSM.AddState(attackState);
        m_FSM.AddState(chaseState);
        m_FSM.AddState(diestate);
        m_FSM.AddState(damagestate);

        AI_ActionBehavior.GetComponet(m_Data);
        //  m_Data.m_HP = 100;

    }

    

    // Update is called once per frame
    void Update()
    {
        AI_ActionBehavior.SetParameterWithTroll(m_Data);
        AI_ActionBehavior.IsMove(m_Data);
        m_FSM.DoState();
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            m_FSM.PerformGlobalTransition(eFSMTransition.Go_Die);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            m_FSM.PerformGlobalTransition(eFSMTransition.Go_Damage);
        }



    }
}
