﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M_IdleState : FSMstate
{
    public float CountTime;
    public M_IdleState()
    {
        m_StateID = eFSMStateID.IdleStateID;
    }


    public override void DoBeforeEnter(Nav_Data data)
    {

    }

    public override void DoBeforeLeave(Nav_Data data)
    {

    }

    public override void Do(Nav_Data data)
    {
        CountTime += Time.deltaTime;
    }

    public override void CheckCondition(Nav_Data data)
    {
        if (CountTime > data.IdleTime)
        {
            //if(AI_ActionBehavior.CheckDis(data))
        }
    }
}

public class M_PatrolState : FSMstate
{

    public M_PatrolState()
    {
        m_StateID = eFSMStateID.PatrolStateID;
    }
    public override void DoBeforeEnter(Nav_Data data)
    {

    }
    public override void DoBeforeLeave(Nav_Data data)
    {

    }
    public override void Do(Nav_Data data)
    {
        AI_ActionBehavior.DoPatrol(data, data.TrollWalkSpeed);

    }
    public override void CheckCondition(Nav_Data data)
    {

        if (AI_ActionBehavior.IsMove(data))
        {
            if (AI_ActionBehavior.CheckDis(data) < data.AlertRange)
            {
                data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Alert);
            }
        }
    }
}

public class M_AlertState : FSMstate
{
    private float CountTime;
    public M_AlertState()
    {
        m_StateID = eFSMStateID.AlertStateID;
        CountTime = 0.0f;
    }
    public override void DoBeforeEnter(Nav_Data data)
    {
        CountTime = 0.0f;
    }
    public override void DoBeforeLeave(Nav_Data data)
    {
        CountTime = 0.0f;
    }
    public override void Do(Nav_Data data)
    {
        AI_ActionBehavior.Doalert(data);
        CountTime += Time.deltaTime;

    }
    public override void CheckCondition(Nav_Data data)
    {

        if (CountTime > data.AlertTime)
        {
            if (AI_ActionBehavior.CheckDis(data) > data.AlertRange)
            {
                data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Patrol);
            }
            else
            {
                data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Chase);
            }
        }
        else if (AI_ActionBehavior.CheckDis(data) < data.ChaseRange)
        {
            data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Chase);
        }
    }

}


public class M_ChaseState : FSMstate
{
    public float CountTime;
    public M_ChaseState()
    {
        m_StateID = eFSMStateID.ChaseStateID;
        CountTime = 0.0f;
    }
    public override void DoBeforeEnter(Nav_Data data)
    {
        CountTime = 0.0f;
    }
    public override void DoBeforeLeave(Nav_Data data)
    {
        CountTime = 0.0f;

    }
    public override void Do(Nav_Data data)
    {
        AI_ActionBehavior.Dochase(data, data.TrollRunSpeed);
        CountTime += Time.deltaTime;
        data.agent.stoppingDistance = 6;

    }
    public override void CheckCondition(Nav_Data data)
    {

        if (CountTime > data.ChaseTime)
        {
            if (AI_ActionBehavior.CheckDis(data) > data.ChaseRange)
            {
                data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Patrol);
            }
            else if (AI_ActionBehavior.CheckDis(data) < data.AttackRange)
            {
                data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Attack);
            }

        }
        else if (AI_ActionBehavior.CheckDis(data) < data.AttackRange)
        {
            data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Attack);
        }

    }
}

public class M_AttackState : FSMstate
{
    public float CountTime;
    public float HitboxTime;
    public M_AttackState()
    {
        m_StateID = eFSMStateID.AttackStateID;
        CountTime = 0;
        HitboxTime = 0;
    }
    public override void DoBeforeEnter(Nav_Data data)
    {
        CountTime = 0;
        HitboxTime = 0;
    }
    public override void DoBeforeLeave(Nav_Data data)
    {

    }
    public override void Do(Nav_Data data)
    {           
           CountTime += Time.deltaTime;
        if (CountTime > data.AttackTime && AI_ActionBehavior.isTurnToTarget(data) == false)
        {
            AI_ActionBehavior.TurntoTarget(data);            
        }
        else if (CountTime > data.AttackTime && AI_ActionBehavior.isTurnToTarget(data) == true)
        {
            AI_ActionBehavior.DoAttack(data);           
            HitboxTime = Time.time;
            CountTime = 0;             
        }

        if (Time.time - HitboxTime >0.8f && Time.time - HitboxTime < 1.2f)
        {
            AI_ActionBehavior.SetTrollAttackHitBox(data, true);
        }
        else
        {
            AI_ActionBehavior.SetTrollAttackHitBox(data, false);
        }
    }
    public override void CheckCondition(Nav_Data data)
    {

        if (AI_ActionBehavior.CheckDis(data) > data.AttackRange)
        {
            data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Chase);
        }


    }
}

public class M_DamageState : FSMstate
{
    public float CountTime;
    public M_DamageState()
    {
        m_StateID = eFSMStateID.DamageStateID;
        CountTime = 0;
    }
    public override void DoBeforeEnter(Nav_Data data)
    {
        CountTime = 0;
    }
    public override void DoBeforeLeave(Nav_Data data)
    {
        CountTime = 0;
    }
    public override void Do(Nav_Data data)
    {
        CountTime += Time.time;
        AI_ActionBehavior.DoDamaged(data);
        Debug.Log("123");
    }
    public override void CheckCondition(Nav_Data data)
    {
        if (CountTime > 0.5f)
        {
            if ((data.m_FSMSystem.CheckPreviousStateID()) == eFSMStateID.ChaseStateID)
            {
                data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Chase);
                CountTime = 0;
            }
            if ((data.m_FSMSystem.CheckPreviousStateID()) == eFSMStateID.AttackStateID)
            {
                data.m_FSMSystem.PerformTransition(eFSMTransition.Go_Attack);
                CountTime = 0;
            }
        }
    }
}
public class M_DieState :FSMstate
{
    public M_DieState()
    {
        m_StateID = eFSMStateID.DamageStateID;

    }
    public override void DoBeforeEnter(Nav_Data data)
    {

    }
    public override void DoBeforeLeave(Nav_Data data)
    {

    }
    public override void Do(Nav_Data data)
    {
        AI_ActionBehavior.DoDie(data);


    }
    public override void CheckCondition(Nav_Data data)
    {
       
    }
}

