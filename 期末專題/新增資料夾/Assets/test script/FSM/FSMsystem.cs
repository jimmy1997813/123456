﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class FSMsystem

{
    private List<FSMstate> m_state;
    private Dictionary<eFSMTransition, FSMstate> m_GlobalMap;
    private eFSMStateID m_currentStateID;
    public eFSMStateID CurrentStateID { get { return m_currentStateID; } }
    private FSMstate m_currentState;
    public FSMstate CurrentState { get { return m_currentState; } }
    private eFSMStateID m_previousstateID;
    public eFSMStateID PreviousstateID { get { return m_previousstateID; } }
    private Nav_Data m_Data;
    public FSMsystem(Nav_Data data)
    {
        m_Data = data;
        m_state = new List<FSMstate>();
        m_GlobalMap = new Dictionary<eFSMTransition, FSMstate>();
    }
    public void AddGlobalTransition(eFSMTransition t, FSMstate s)
    {
        m_GlobalMap.Add(t, s);
    }

    public void PerformGlobalTransition(eFSMTransition t)
    {
        if (m_GlobalMap.ContainsKey(t))
        {
            m_currentState.DoBeforeLeave(m_Data);
            m_previousstateID = m_currentStateID;
            m_currentState = m_GlobalMap[t];
            m_currentState.DoBeforeEnter(m_Data);
            m_currentStateID = m_currentState.m_StateID;
        }
    }
    public eFSMStateID CheckPreviousStateID()
    {
        return m_previousstateID;
    }
    public void AddState(FSMstate s)
    {
        if(s == null)
        {
            return;
        }
        if(m_state.Count == 0)
        {
            m_state.Add(s);
            m_currentState = s;
            m_currentStateID = s.m_StateID;
        }
        foreach(FSMstate state in m_state)
        {
            if(state.m_StateID == s.m_StateID)
            {
                return;
            }
        }
        m_state.Add(s);
    }
    public void DeleteState(eFSMStateID id)
    {
        if(id == eFSMStateID.Null_StateID)
        {
            return;
        }
        foreach(FSMstate state in m_state)
        {
            if(state.m_StateID == id)
            {
                m_state.Remove(state);
                return;
            }
        }
    }
    public void PerformTransition(eFSMTransition trans)
    {
        if (trans == eFSMTransition.Null_transition)
        {
            return;
        }

        FSMstate state = m_currentState.TransitionState(trans);
        if (state == null)
        {
            return;
        }

        // Update the currentStateID and currentState		
        m_currentState.DoBeforeLeave(m_Data);
        
        m_currentState = state;
        m_currentStateID = state.m_StateID;
        Debug.Log("Change to:" + state);
        m_currentState.DoBeforeEnter(m_Data);

    }

    public void DoState()
    {
        m_currentState.CheckCondition(m_Data);
        m_currentState.Do(m_Data);
    }
}
