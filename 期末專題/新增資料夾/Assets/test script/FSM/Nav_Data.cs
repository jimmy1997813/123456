﻿using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class Nav_Data 
{
    public Collider[] Collider;
    public NavMeshAgent agent;
    public GameObject m_object;
    public GameObject m_target;
    public Animator animator;
    public Transform[] points;
    public BoxCollider[] m_hitbox;


    public bool isMove;
    public float MoveSpeed;
    public float t_Distance;
    public float m_HP;

   //transition 
    public float AlertRange ;
    public float ChaseRange;
    public float AttackRange;
    public float d_FightRange;
    public float EyeAngleRange;

    public float DragonWalkSpeed;

    public float TrollRunSpeed;
    public float TrollWalkSpeed;

    public float TurnSpeed;

    public float IdleTime;
    public float AlertTime ;
    public float ChaseTime;
    public float AttackTime ;
    public int destPoint ;

    public FSMsystem m_FSMSystem;



    public  Nav_Data()
    {
       
    }



}


