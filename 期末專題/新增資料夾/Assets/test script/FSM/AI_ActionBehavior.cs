﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AI_ActionBehavior 
{
    /// <summary>
    /// 檢查是否移動
    /// </summary>
    /// <param name="data"></param>
    static public bool IsMove(Nav_Data data)
    {
        data.agent.speed = data.MoveSpeed;
        if (data.MoveSpeed != 0)
        {
            data.isMove = true;
            return true;
        }
        else
        {
            data.isMove = false;
            return false;
        }
        
    }
    /// <summary>
    /// 停
    /// </summary>
    /// <param name="data"></param>
    static public void AI_Idle(Nav_Data data)
    {
        data.MoveSpeed = 0;  
    }
    /// <summary>
    /// 跑
    /// </summary>
    /// <param name="data"></param>
    static public void AI_RUN(Nav_Data data,float RunSpeed)
    {
        data.MoveSpeed = RunSpeed;

    }
    /// <summary>
    /// 走
    /// </summary>
    /// <param name="data"></param>
    static public void AI_Walk(Nav_Data data,float WalkSpeed)
    {
        data.MoveSpeed = WalkSpeed;

    }

    /// <summary>
    /// 轉向目標
    /// </summary>
    /// <param name="data"></param>
    static public void TurntoTarget(Nav_Data data)
    {   

        Vector3 TargetDir = (data.m_target.transform.position - data.m_object.transform.position).normalized;
        float SingleStep = data.TurnSpeed * Time.deltaTime;
        float angle = Vector3.Angle(data.m_object.transform.forward, TargetDir);
        float needtime = angle / SingleStep;
        Quaternion t = Quaternion.LookRotation(TargetDir, Vector3.up);
        data.m_object.transform.rotation = Quaternion.Slerp(data.m_object.transform.rotation, t, SingleStep);
        //Vector3 newDir = Vector3.RotateTowards(data.m_target.transform.forward, TargetDir, SingleStep, 0.0f);
        //data.m_object.transform.rotation = Quaternion.LookRotation(newDir);
    }
    /// <summary>
    /// 計算目標是否在視野角度內
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    static public bool isTurnToTarget(Nav_Data data)
    {
        Vector3 srcLocalVect = data.m_target.transform.position - data.m_object.transform.position;
        srcLocalVect.y = 0;
        Vector3 forwardLocalPos = data.m_object.transform.forward * 1 + data.m_object.transform.position;
        Vector3 forwardLocalVect = forwardLocalPos - data.m_object.transform.position;
        forwardLocalVect.y = 0;
        float angle = Vector3.Angle(srcLocalVect, forwardLocalVect);
        if ( angle < data.EyeAngleRange / 2)
        {
            return true;
        }        
        else
        {
            return false;
        }
    }
       /// <summary>
    /// 觸發攻擊動畫
    /// </summary>
    /// <param name="data"></param>
    static public void AttackTrigger(Nav_Data data)
    {
        Animator ani = data.animator;
        ani.SetTrigger("isAttack");
       
    }
    /// <summary>
    /// 取得元件(boxcolider[]),初始化不顯示,目前[0]
    /// </summary>
    /// <param name="data"></param>
    static public void GetComponet(Nav_Data data)
    {
        data.m_hitbox = data.m_object.GetComponentsInChildren<BoxCollider>();
        data.m_hitbox[0].enabled = false;
    }

    /// <summary>
    /// Troll,使AI的變數可以改變animator 
    /// </summary>
    /// <param name="data"></param>
    static public void SetParameterWithTroll(Nav_Data data)
    {
        Animator ani = data.animator;
        
        data.t_Distance = Vector3.Distance(data.m_object.transform.position, data.m_target.transform.position);
        ani.SetFloat("t_Distance", data.t_Distance);
        ani.SetFloat("MoveSpeed", data.MoveSpeed);
        ani.SetBool("isMove", data.isMove);

    }
    /// <summary>
    ///檢查距離
    /// </summary>
    /// <param name="data"></param>
    /// <param name="dis"></param>
    static public float CheckDis(Nav_Data data)
    {
        data.t_Distance = Vector3.Distance(data.m_object.transform.position, data.m_target.transform.position);
        return data.t_Distance;
    }
    /// <summary>
    /// 巡邏狀態
    /// </summary>
    /// <param name="data"></param>
    static public void DoPatrol(Nav_Data data,float WalkSpeed)
    {        
            AI_ActionBehavior.AI_Walk(data,WalkSpeed);
        data.agent.destination = data.points[data.destPoint].position;

        //是否正在計算&距離巡邏點前?
        if (!data.agent.pathPending && data.agent.remainingDistance < 3)
                {
                    if (data.points.Length == 0) return;
                    data.agent.destination = data.points[data.destPoint].position;
                    data.destPoint = (data.destPoint + 1) % data.points.Length;
                }               
    }
    /// <summary>
    /// 警戒狀態
    /// </summary>
    /// <param name="data"></param>
    /// <param name="Betwen_Distance"></param>
    static public void Doalert(Nav_Data data)
    {
            TurntoTarget(data);
            data.agent.destination = data.m_target.transform.position;
            AI_Idle(data);
    }
    /// <summary>
    /// 追擊狀態
    /// </summary>
    /// <param name="data"></param>
    static public void Dochase(Nav_Data data,float ChaseSpeed)
    {
        data.agent.destination = data.m_target.transform.position;
        AI_RUN(data,ChaseSpeed);


    }
    /// <summary>
    /// 攻擊狀態
    /// </summary>
    /// <param name="data"></param>
    static public void DoAttack(Nav_Data data)
    {
        AttackTrigger(data);
        AI_Idle(data);
    }
    /// <summary>
    /// 執行死亡動畫
    /// </summary>
    /// <param name="data"></param>
    static public void DoDie(Nav_Data data)
    {
        Animator ani = data.animator;
        ani.SetTrigger("isDie");
    }
    /// <summary>
    /// 執行受傷動畫
    /// </summary>
    /// <param name="data"></param>
    static public void DoDamaged(Nav_Data data)
    {
        Animator ani = data.animator;
        ani.SetTrigger("Hit_trigger");
    }
    /// <summary>
    /// 設置攻擊hitbox
    /// </summary>
    /// <param name="data"></param>
    static public void SetTrollAttackHitBox(Nav_Data data,bool oc)
    {
        if (oc == true)
        {
            data.m_hitbox[0].enabled = true;
        }
        else if(oc == false)
        {
            data.m_hitbox[0].enabled = false;
        }
    }




}
