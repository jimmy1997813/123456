﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum eFSMTransition
{
    Null_transition = 0,
    Go_Idle,
    Go_Sleep,
    Go_Patrol,
    Go_Alert,
    Go_Chase,
    Go_Attack,
    Go_Fight,
    Go_Damage,
    Go_Die
    
}
public enum eFSMStateID
{
    Null_StateID = 0,
    IdleStateID,
    SleepStateID,
    PatrolStateID,
    AlertStateID,
    ChaseStateID,
    AttackStateID,
    FightStateID,
    DamageStateID,
    DieStateID
}

public class FSMstate
{
    public eFSMStateID m_StateID;
    public Dictionary<eFSMTransition, FSMstate> m_Map;

    public FSMstate()
    {
        m_StateID = eFSMStateID.Null_StateID;
        m_Map = new Dictionary<eFSMTransition, FSMstate>();

    }

    public void AddTransition(eFSMTransition trans,FSMstate state)
    {
        if (m_Map.ContainsKey(trans) == true)
        {
            return;
        }
        else
        {
            m_Map.Add(trans, state);
        }
    }

    public void DeleteTransition(eFSMTransition trans)
    {
        if (m_Map.ContainsKey(trans) == true)
        {
            m_Map.Remove(trans);
        }
    }

    public FSMstate TransitionState(eFSMTransition trans)
    {
        if (m_Map.ContainsKey(trans) == false)
        {
            return null;
        }
        return m_Map[trans];
    }

    public virtual void DoBeforeEnter(Nav_Data data)
    {

    }

    public virtual void DoBeforeLeave(Nav_Data data)
    {

    }

    public virtual void Do(Nav_Data data)
    {
       
    }

    public virtual void CheckCondition(Nav_Data data)
    {

    }

    public virtual void RecordPreviousState(Nav_Data data)
    {

    }


}

